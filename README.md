# SM-EXEC_article
[SM.EXEC: A Network Service Logic Execution Environment Architecture](https://www.researchgate.net/publication/379564545_SMEXEC_A_Network_Service_Logic_Execution_Environment_Architecture)

[![License: CC BY-NC-ND 4.0](https://img.shields.io/badge/License-CC%20BY--NC--ND%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-nc-nd/4.0/)
